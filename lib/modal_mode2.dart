import 'package:flutter/material.dart';
import 'package:lab_modal/modal_mode2_view3.dart';

class ModalMode2 extends StatelessWidget {
  const ModalMode2({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: const Text('Vista 1')),
        body: const ModalMode(),
      ),
    );
  }
}

class ModalMode extends StatefulWidget {
  const ModalMode({super.key});

  @override
  State<StatefulWidget> createState() => _ModalMode();
}

class _ModalMode extends State<ModalMode> {
  var example = "";

  examples(context, test) {
    setState(() {
      example = "Opción seleccionada: $test";
    });
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ModalMode2_View3(example),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
      height: 200,
      width: 500,
      child: Column(
        children: [
          ElevatedButton(
            child: const Text('Mostrar Opciones'),
            onPressed: () {
              showModalBottomSheet<void>(
                context: context,
                builder: (context) {
                  return Container(
                    height: 1000,
                    color: Colors.lightBlue[50],
                    child: Column(
                      children: [
                        const Text('Elija su tipo de Mate Favorito'),
                        Column(children: [
                          ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.lightBlueAccent),
                              onPressed: () => {examples(context, 'Playadito')},
                              child: Text('Playadito')),
                          ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.greenAccent),
                              onPressed: () => {examples(context, 'La Merced')},
                              child: Text('La Merced')),
                          ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.redAccent),
                              onPressed: () => {examples(context, 'Rosamonte')},
                              child: Text('RosaMonte'))
                        ])
                      ],
                    ),
                  );
                },
              );
            },
          ),
        ],
      ),
    ));
  }
}
