import 'package:flutter/material.dart';

class ModalMode2_View3 extends StatelessWidget {
  String selected;
  ModalMode2_View3(this.selected, {super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: const Text('Vista 3')),
        body: ModalMode(selected),
      ),
    );
  }
}

class ModalMode extends StatefulWidget {
  String selected;
  ModalMode(this.selected, {super.key});

  @override
  State<StatefulWidget> createState() => _ModalMode();
}

class _ModalMode extends State<ModalMode> {
  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
      height: 200,
      width: 500,
      child: Column(
        children: [
          Text(widget.selected),
        ],
      ),
    ));
  }
}
