import 'package:flutter/material.dart';

// stay permanent
MateTypes? _mate = MateTypes.reiVerde;
String tipo = "Rei Verde";

class ModalMode1 extends StatelessWidget {
  const ModalMode1({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(title: const Text('Vista 1')),
        body: const ModalMode(),
      ),
    );
  }
}

class ModalMode extends StatefulWidget {
  const ModalMode({super.key});

  @override
  State<StatefulWidget> createState() => _ModalMode();
}

class _ModalMode extends State<ModalMode> {
  String mate = "";
  keepInformation(parametro) {
    setState(() {
      mate = parametro;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Container(
      width: 500,
      height: 200,
      child: Column(
        children: [
          ElevatedButton(
            child: const Text('Mostrar Opciones'),
            onPressed: () {
              showModalBottomSheet<void>(
                context: context,
                builder: (context) {
                  return Container(
                    height: 1000,
                    color: Colors.lightBlue[50],
                    child: Column(
                      children: const [
                        Text('Elija su tipo de Mate Favorito'),
                        RadioMateOptions(),
                      ],
                    ),
                  );
                },
              ).whenComplete(() => {keepInformation(tipo)});
            },
          ),
          Text("Opción seleccionada: $mate"),
        ],
      ),
    ));
  }
}

enum MateTypes { reiVerde, laMerced, taragui, rosaMonte, playadito }

class RadioMateOptions extends StatefulWidget {
  const RadioMateOptions({super.key});

  @override
  State<StatefulWidget> createState() => _RadioMateOptionsState();
}

class _RadioMateOptionsState extends State<RadioMateOptions> {
  sendData(context) {
    Navigator.pop(context, tipo);
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        RadioListTile(
          title: const Text('Rei Verde'),
          value: MateTypes.reiVerde,
          groupValue: _mate,
          onChanged: (MateTypes? value) {
            setState(() {
              _mate = value;
              tipo = 'Rei Verde';
            });
          },
        ),
        RadioListTile(
          title: const Text('La Merced'),
          value: MateTypes.laMerced,
          groupValue: _mate,
          onChanged: (MateTypes? value) {
            setState(() {
              _mate = value;
              tipo = 'La Merced';
            });
          },
        ),
        RadioListTile(
          title: const Text('Taragui'),
          value: MateTypes.taragui,
          groupValue: _mate,
          onChanged: (MateTypes? value) {
            setState(() {
              _mate = value;
              tipo = 'Taragui';
            });
          },
        ),
        RadioListTile(
          title: const Text('Playadito'),
          value: MateTypes.playadito,
          groupValue: _mate,
          onChanged: (MateTypes? value) {
            setState(() {
              _mate = value;
              tipo = 'Playadito';
            });
          },
        ),
        ElevatedButton(
          child: const Text('Aceptar'),
          onPressed: () => sendData(context),
        ),
      ],
    );
  }
}
